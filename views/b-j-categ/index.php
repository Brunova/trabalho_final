<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BJCategSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cadastrar:';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bjcateg-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Names',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
