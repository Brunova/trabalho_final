<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BJCateg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bjcateg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Names')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salve', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
