<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BJCateg */

$this->title = 'Update Bj Categ: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Bj Categs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bjcateg-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
