<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BJCateg */

$this->title = 'Create Bj Categ';
$this->params['breadcrumbs'][] = ['label' => 'Bj Categs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bjcateg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
