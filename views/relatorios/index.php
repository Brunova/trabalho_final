<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Total de cupons', ['relatorio1'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Cupons de eletrôicos', ['relatorio2'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Cupons na NTC', ['relatorio3'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Cupons na Americanas', ['relatorio4'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Cupons na Submarino', ['relatorio5'], ['class' => 'btn btn-success']) ?>

 
</div>
