<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BJCupons */

$this->title = 'Prencha os dados';
$this->params['breadcrumbs'][] = ['label' => 'Bj Cupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bjcupons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
