<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\BJStores;
use app\models\BJCateg;


/* @var $this yii\web\View */
/* @var $model app\models\BJCupons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bjcupons-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Cupom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Data')->textInput(['type'=>'date']) ?> 

    <?= $form->field($model, 'ID_stores')->
       dropDownList(ArrayHelper::map(BJStores::find()
           ->orderBy('Names')
           ->all(),'ID','Names'),
           ['prompt' => 'Selecione uma loja'] )
    ?>

    <?= $form->field($model, 'Validity')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'Categ')->
       dropDownList(ArrayHelper::map(BJCateg::find()
           ->orderBy('Names')
           ->all(),'ID','Names'),
           ['prompt' => 'Selecione uma categoria'] )
    ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
