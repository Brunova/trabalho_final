<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BJCuponsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bjcupons-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Cupom') ?>

    <?= $form->field($model, 'Data') ?>

    <?= $form->field($model, 'ID_stores') ?>

    <?= $form->field($model, 'Validity') ?>

    <?php // echo $form->field($model, 'Categ') ?>

    <div class="form-group">
        <?= Html::submitButton('Procurar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Recaregar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
