<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BJCuCa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bjcu-ca-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_Cu')->textInput() ?>

    <?= $form->field($model, 'ID_Ca')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
