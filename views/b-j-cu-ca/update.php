<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BJCuCa */

$this->title = 'Update Bj Cu Ca: ' . $model->ID_Cu;
$this->params['breadcrumbs'][] = ['label' => 'Bj Cu Cas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_Cu, 'url' => ['view', 'ID_Cu' => $model->ID_Cu, 'ID_Ca' => $model->ID_Ca]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bjcu-ca-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
