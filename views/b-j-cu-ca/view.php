<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BJCuCa */

$this->title = $model->ID_Cu;
$this->params['breadcrumbs'][] = ['label' => 'Bj Cu Cas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bjcu-ca-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'ID_Cu' => $model->ID_Cu, 'ID_Ca' => $model->ID_Ca], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'ID_Cu' => $model->ID_Cu, 'ID_Ca' => $model->ID_Ca], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_Cu',
            'ID_Ca',
        ],
    ]) ?>

</div>
