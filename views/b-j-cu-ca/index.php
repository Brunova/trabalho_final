<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BJCuCaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bj Cu Cas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bjcu-ca-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bj Cu Ca', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID_Cu',
            'ID_Ca',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
