<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BJStores */

$this->title = 'Create Bj Stores';
$this->params['breadcrumbs'][] = ['label' => 'Bj Stores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bjstores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
