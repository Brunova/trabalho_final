<?php

namespace app\controllers;

use Yii;
use app\models\BJCuCa;
use app\models\BJCuCaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BJCuCaController implements the CRUD actions for BJCuCa model.
 */
class BJCuCaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BJCuCa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BJCuCaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BJCuCa model.
     * @param integer $ID_Cu
     * @param integer $ID_Ca
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($ID_Cu, $ID_Ca)
    {
        return $this->render('view', [
            'model' => $this->findModel($ID_Cu, $ID_Ca),
        ]);
    }

    /**
     * Creates a new BJCuCa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BJCuCa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_Cu' => $model->ID_Cu, 'ID_Ca' => $model->ID_Ca]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BJCuCa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ID_Cu
     * @param integer $ID_Ca
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($ID_Cu, $ID_Ca)
    {
        $model = $this->findModel($ID_Cu, $ID_Ca);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ID_Cu' => $model->ID_Cu, 'ID_Ca' => $model->ID_Ca]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BJCuCa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $ID_Cu
     * @param integer $ID_Ca
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($ID_Cu, $ID_Ca)
    {
        $this->findModel($ID_Cu, $ID_Ca)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BJCuCa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $ID_Cu
     * @param integer $ID_Ca
     * @return BJCuCa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ID_Cu, $ID_Ca)
    {
        if (($model = BJCuCa::findOne(['ID_Cu' => $ID_Cu, 'ID_Ca' => $ID_Ca])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
