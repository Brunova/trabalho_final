-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 12-Dez-2019 às 11:05
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `trabalhofinal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_categ`
--

CREATE TABLE `bj_categ` (
  `ID` int(11) NOT NULL,
  `Names` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_cuca`
--

CREATE TABLE `bj_cuca` (
  `ID_Cu` int(11) NOT NULL,
  `ID_Ca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_cupons`
--

CREATE TABLE `bj_cupons` (
  `ID` int(11) NOT NULL,
  `Cupom` varchar(200) NOT NULL,
  `Data` date DEFAULT NULL,
  `ID_stores` int(11) DEFAULT NULL,
  `Validity` varchar(22) NOT NULL,
  `Categ` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_stores`
--

CREATE TABLE `bj_stores` (
  `ID` int(11) NOT NULL,
  `Names` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `bj_stores`
--

INSERT INTO `bj_stores` (`ID`, `Names`) VALUES
(1, 'Americanas'),
(2, 'Submario'),
(3, 'KaBuM!'),
(4, 'Magazine Luiza'),
(5, 'Casas Bahia'),
(6, 'Fast Shop'),
(7, 'Pontofrio'),
(8, 'Extra'),
(9, 'Shoptime'),
(10, 'Livraria Saraiva'),
(11, 'Ricardo Eletro'),
(12, 'Lojas Centauro'),
(13, 'C&A'),
(14, 'Polishop'),
(15, 'O Boticário'),
(16, 'Natura'),
(17, 'NTC - NÃO TEM COMO COMERCIO DE ROUPAS E ACESSÓRIOS'),
(18, 'Fallen Store'),
(19, 'Eletrosom'),
(20, 'Zema'),
(21, 'Netshoes'),
(22, 'Zattini'),
(23, 'Adidas'),
(24, 'Nike'),
(25, 'Supreme'),
(26, 'Lojinha do Seu Zé');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `bj_categ`
--
ALTER TABLE `bj_categ`
  ADD PRIMARY KEY (`ID`);

--
-- Índices para tabela `bj_cuca`
--
ALTER TABLE `bj_cuca`
  ADD PRIMARY KEY (`ID_Cu`,`ID_Ca`),
  ADD KEY `ID_Ca` (`ID_Ca`);

--
-- Índices para tabela `bj_cupons`
--
ALTER TABLE `bj_cupons`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_stores` (`ID_stores`);

--
-- Índices para tabela `bj_stores`
--
ALTER TABLE `bj_stores`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `bj_categ`
--
ALTER TABLE `bj_categ`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `bj_cupons`
--
ALTER TABLE `bj_cupons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `bj_stores`
--
ALTER TABLE `bj_stores`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `bj_cuca`
--
ALTER TABLE `bj_cuca`
  ADD CONSTRAINT `bj_cuca_ibfk_1` FOREIGN KEY (`ID_Cu`) REFERENCES `bj_cupons` (`ID`),
  ADD CONSTRAINT `bj_cuca_ibfk_2` FOREIGN KEY (`ID_Ca`) REFERENCES `bj_categ` (`ID`);

--
-- Limitadores para a tabela `bj_cupons`
--
ALTER TABLE `bj_cupons`
  ADD CONSTRAINT `bj_cupons_ibfk_1` FOREIGN KEY (`ID_stores`) REFERENCES `bj_stores` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
