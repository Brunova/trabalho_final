-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Dez-2019 às 14:25
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trabalhofinal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_categ`
--

CREATE TABLE `bj_categ` (
  `ID` int(11) NOT NULL,
  `Names` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `bj_categ`
--

INSERT INTO `bj_categ` (`ID`, `Names`) VALUES
(1, 'Eletrodoméstico '),
(2, 'Ferramentas'),
(3, 'Esportes'),
(4, 'Games'),
(5, 'Informática'),
(7, 'Livros'),
(8, 'Câmeras'),
(9, 'instrumentos'),
(10, 'Decoração'),
(11, 'Alimentos '),
(12, 'Brinquedos '),
(13, 'Eletroportáteis'),
(14, 'joias'),
(15, 'Suplementos Alimentares');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bj_categ`
--
ALTER TABLE `bj_categ`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bj_categ`
--
ALTER TABLE `bj_categ`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
