<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "BJ_CuCa".
 *
 * @property int $ID_Cu
 * @property int $ID_Ca
 */
class BJCuCa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'BJ_CuCa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_Cu', 'ID_Ca'], 'required'],
            [['ID_Cu', 'ID_Ca'], 'integer'],
            [['ID_Cu', 'ID_Ca'], 'unique', 'targetAttribute' => ['ID_Cu', 'ID_Ca']],
            [['ID_Cu'], 'exist', 'skipOnError' => true, 'targetClass' => BjCupons::className(), 'targetAttribute' => ['ID_Cu' => 'ID']],
            [['ID_Ca'], 'exist', 'skipOnError' => true, 'targetClass' => BjCateg::className(), 'targetAttribute' => ['ID_Ca' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_Cu' => 'Id Cu',
            'ID_Ca' => 'Id Ca',
        ];
    }
}
