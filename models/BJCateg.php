<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "BJ_categ".
 *
 * @property int $ID
 * @property string $Names
 */
class BJCateg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'BJ_categ';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Names'], 'required'],
            [['Names'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Names' => 'Names',
        ];
    }
}
