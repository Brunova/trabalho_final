<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bj_cupons".
 *
 * @property int $ID
 * @property string $Cupom
 * @property string $Data
 * @property int $ID_stores
 * @property string $Validity
 * @property string $Categ
 *
 * @property BjCuca[] $bjCucas
 * @property BjCateg[] $cas
 * @property BjStores $stores
 */
class BjCupons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bj_cupons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Cupom', 'Validity', 'Categ'], 'required'],
            [['Data'], 'safe'],
            [['ID_stores'], 'integer'],
            [['Cupom'], 'string', 'max' => 200],
            [['Validity'], 'string', 'max' => 22],
            [['Categ'], 'string', 'max' => 50],
            [['ID_stores'], 'exist', 'skipOnError' => true, 'targetClass' => BjStores::className(), 'targetAttribute' => ['ID_stores' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Cupom' => 'Cupom',
            'Data' => 'Data',
            'ID_stores' => 'Loja',
            'Validity' => 'Validade',
            'Categ' => 'Categoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBjCucas()
    {
        return $this->hasMany(BjCuca::className(), ['ID_Cu' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCas()
    {
        return $this->hasMany(BjCateg::className(), ['ID' => 'ID_Ca'])->viaTable('bj_cuca', ['ID_Cu' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasOne(BjStores::className(), ['ID' => 'ID_stores']);
    }
}
