<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BJCupons;

/**
 * BJCuponsSearch represents the model behind the search form of `app\models\BJCupons`.
 */
class BJCuponsSearch extends BJCupons
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'ID_stores'], 'integer'],
            [['Cupom', 'Data', 'Validity', 'Categ'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BJCupons::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Data' => $this->Data,
            'ID_stores' => $this->ID_stores,
        ]);

        $query->andFilterWhere(['like', 'Cupom', $this->Cupom])
            ->andFilterWhere(['like', 'Validity', $this->Validity])
            ->andFilterWhere(['like', 'Categ', $this->Categ]);

        return $dataProvider;
    }
}
