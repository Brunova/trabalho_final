-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12-Dez-2019 às 13:50
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trabalhofinal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncio`
--

CREATE TABLE `anuncio` (
  `id_anuncio` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `salario` decimal(10,2) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `numero_vagas` int(11) NOT NULL,
  `carga_horaria` time NOT NULL,
  `pessoa_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `local_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_categ`
--

CREATE TABLE `bj_categ` (
  `ID` int(11) NOT NULL,
  `Names` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_cuca`
--

CREATE TABLE `bj_cuca` (
  `ID_Cu` int(11) NOT NULL,
  `ID_Ca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_cupons`
--

CREATE TABLE `bj_cupons` (
  `ID` int(11) NOT NULL,
  `Cupom` varchar(200) NOT NULL,
  `Data` date DEFAULT NULL,
  `ID_stores` int(11) DEFAULT NULL,
  `Validity` varchar(22) NOT NULL,
  `Categ` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bj_stores`
--

CREATE TABLE `bj_stores` (
  `ID` int(11) NOT NULL,
  `Names` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `tipo_de_serviço` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `nome_empresa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `local`
--

CREATE TABLE `local` (
  `id_local` int(11) NOT NULL,
  `nome_local` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_anuncio`
--

CREATE TABLE `mg_anuncio` (
  `id_anuncio` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `salario` decimal(10,2) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `numero_vagas` int(11) NOT NULL,
  `carga_horaria` time NOT NULL,
  `pessoa_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `local_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_categoria`
--

CREATE TABLE `mg_categoria` (
  `id_categoria` int(11) NOT NULL,
  `tipo_de_serviço` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mg_categoria`
--

INSERT INTO `mg_categoria` (`id_categoria`, `tipo_de_serviço`) VALUES
(1, 'atendente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_empresa`
--

CREATE TABLE `mg_empresa` (
  `id_empresa` int(11) NOT NULL,
  `nome_empresa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mg_empresa`
--

INSERT INTO `mg_empresa` (`id_empresa`, `nome_empresa`) VALUES
(1, 'Empresa BH');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_local`
--

CREATE TABLE `mg_local` (
  `id_local` int(11) NOT NULL,
  `nome_local` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mg_local`
--

INSERT INTO `mg_local` (`id_local`, `nome_local`) VALUES
(1, 'BH');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mg_pessoa`
--

CREATE TABLE `mg_pessoa` (
  `id_pessoa` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `contato` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mg_pessoa`
--

INSERT INTO `mg_pessoa` (`id_pessoa`, `nome`, `contato`) VALUES
(1, 'Marques', '38 998850212');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id_pessoa` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `contato` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anuncio`
--
ALTER TABLE `anuncio`
  ADD PRIMARY KEY (`id_anuncio`),
  ADD KEY `pessoa_id` (`pessoa_id`),
  ADD KEY `categoria_id` (`categoria_id`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Indexes for table `bj_categ`
--
ALTER TABLE `bj_categ`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `bj_cuca`
--
ALTER TABLE `bj_cuca`
  ADD PRIMARY KEY (`ID_Cu`,`ID_Ca`),
  ADD KEY `ID_Ca` (`ID_Ca`);

--
-- Indexes for table `bj_cupons`
--
ALTER TABLE `bj_cupons`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_stores` (`ID_stores`);

--
-- Indexes for table `bj_stores`
--
ALTER TABLE `bj_stores`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indexes for table `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id_local`);

--
-- Indexes for table `mg_anuncio`
--
ALTER TABLE `mg_anuncio`
  ADD PRIMARY KEY (`id_anuncio`),
  ADD KEY `pessoa_id` (`pessoa_id`),
  ADD KEY `categoria_id` (`categoria_id`),
  ADD KEY `local_id` (`local_id`),
  ADD KEY `empresa_id` (`empresa_id`);

--
-- Indexes for table `mg_categoria`
--
ALTER TABLE `mg_categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `mg_empresa`
--
ALTER TABLE `mg_empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indexes for table `mg_local`
--
ALTER TABLE `mg_local`
  ADD PRIMARY KEY (`id_local`);

--
-- Indexes for table `mg_pessoa`
--
ALTER TABLE `mg_pessoa`
  ADD PRIMARY KEY (`id_pessoa`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id_pessoa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anuncio`
--
ALTER TABLE `anuncio`
  MODIFY `id_anuncio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bj_categ`
--
ALTER TABLE `bj_categ`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bj_cupons`
--
ALTER TABLE `bj_cupons`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bj_stores`
--
ALTER TABLE `bj_stores`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `local`
--
ALTER TABLE `local`
  MODIFY `id_local` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mg_anuncio`
--
ALTER TABLE `mg_anuncio`
  MODIFY `id_anuncio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mg_categoria`
--
ALTER TABLE `mg_categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mg_empresa`
--
ALTER TABLE `mg_empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mg_local`
--
ALTER TABLE `mg_local`
  MODIFY `id_local` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mg_pessoa`
--
ALTER TABLE `mg_pessoa`
  MODIFY `id_pessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id_pessoa` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `anuncio`
--
ALTER TABLE `anuncio`
  ADD CONSTRAINT `anuncio_ibfk_1` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id_pessoa`),
  ADD CONSTRAINT `anuncio_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id_categoria`),
  ADD CONSTRAINT `anuncio_ibfk_3` FOREIGN KEY (`local_id`) REFERENCES `local` (`id_local`),
  ADD CONSTRAINT `anuncio_ibfk_4` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id_empresa`);

--
-- Limitadores para a tabela `bj_cuca`
--
ALTER TABLE `bj_cuca`
  ADD CONSTRAINT `bj_cuca_ibfk_1` FOREIGN KEY (`ID_Cu`) REFERENCES `bj_cupons` (`ID`),
  ADD CONSTRAINT `bj_cuca_ibfk_2` FOREIGN KEY (`ID_Ca`) REFERENCES `bj_categ` (`ID`);

--
-- Limitadores para a tabela `bj_cupons`
--
ALTER TABLE `bj_cupons`
  ADD CONSTRAINT `bj_cupons_ibfk_1` FOREIGN KEY (`ID_stores`) REFERENCES `bj_stores` (`ID`);

--
-- Limitadores para a tabela `mg_anuncio`
--
ALTER TABLE `mg_anuncio`
  ADD CONSTRAINT `mg_anuncio_ibfk_1` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoa` (`id_pessoa`),
  ADD CONSTRAINT `mg_anuncio_ibfk_2` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id_categoria`),
  ADD CONSTRAINT `mg_anuncio_ibfk_3` FOREIGN KEY (`local_id`) REFERENCES `local` (`id_local`),
  ADD CONSTRAINT `mg_anuncio_ibfk_4` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id_empresa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
